# Terragrunt Docker

This project builds a docker image that contains terragrunt and terraform as well as Azure, AWS and Google CLIs.

The docker image is published on Docker Hub at https://hub.docker.com/r/truemark/terragrunt for public consumption.