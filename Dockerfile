FROM ubuntu:18.04

# Install dependencies
RUN apt-get update && apt-get install curl unzip python python-pip apt-transport-https ca-certificates git -y

# Install terraform
RUN curl -L -O https://releases.hashicorp.com/terraform/0.12.12/terraform_0.12.12_linux_amd64.zip && \
    unzip terraform_0.12.12_linux_amd64.zip && \
    chmod +x terraform && mv terraform /usr/local/bin/terraform && \
    rm -f terraform_0.12.12_linux_amd64.zip

# Install terragrunt
RUN curl -L -O https://github.com/gruntwork-io/terragrunt/releases/download/v0.21.0/terragrunt_linux_amd64 && \
    chmod +x terragrunt_linux_amd64 && \
    mv terragrunt_linux_amd64 /usr/local/bin/terragrunt

# Install Azure CLI
RUN pip install azure-cli

# Install AWS CLI
RUN pip install awscli

# Install Google CLI
RUN echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | tee -a /etc/apt/sources.list.d/google-cloud-sdk.list
RUN curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -
RUN apt-get update && apt-get install google-cloud-sdk -y
